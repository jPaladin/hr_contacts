﻿using Contacts.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Contacts.DAL
{
    public class ContactsInitializer : DropCreateDatabaseIfModelChanges<ContactsContext>
    {

        //Seeding database with some data
        protected override void Seed(ContactsContext context)
        {

            var contacts = new List<Contact>
            {
                 new Contact { FirstName = "Josip", LastName = "Paladin", HomeAdress="abcd123", PhoneNumbers = new List<PhoneNumber>(),Tags = new List<Tag>()},
                 new Contact { FirstName = "Darko", LastName = "Rubic", HomeAdress="fdsa123", PhoneNumbers = new List<PhoneNumber>(),Tags = new List<Tag>()},
                 new Contact { FirstName = "Roko", LastName = "Kokan", HomeAdress="321asd", PhoneNumbers = new List<PhoneNumber>(),Tags = new List<Tag>()}
            };

            var phoneNumbers = new List<PhoneNumber>
            {
                 new PhoneNumber {Number = "097123456"},
                 new PhoneNumber {Number = "0991234567"},
                 new PhoneNumber {Number = "091213456"},
                 new PhoneNumber {Number = "0952123136"},

            };

            phoneNumbers.ForEach(p => context.PhoneNumbers.Add(p));
            context.SaveChanges();

            var tags = new List<Tag>
            {
                new Tag { Name = "Posao"},
                new Tag { Name = "Obitelj" },
                new Tag { Name = "Drustvo" },
                new Tag { Name = "Faks" }
            };

            tags.ForEach(t => context.Tags.Add(t));
            context.SaveChanges();

            var emails = new List<Email>
            {
                new Email {EmailAdress = "jp@jp.com" },
                new Email {EmailAdress = "dr@dr.com" },
                new Email {EmailAdress = "rk@rk.com" },
                new Email {EmailAdress = "rk2@rk.com" }
            };

            emails.ForEach(e => context.Emails.Add(e));
            context.SaveChanges();

            //Populating contacts with data
            contacts[0].PhoneNumbers.Add(phoneNumbers[0]);
            contacts[1].PhoneNumbers.Add(phoneNumbers[1]);
            contacts[2].PhoneNumbers.Add(phoneNumbers[2]);
            contacts[2].PhoneNumbers.Add(phoneNumbers[3]);

            contacts[0].Tags.Add(tags[0]);
            contacts[1].Tags.Add(tags[1]);
            contacts[2].Tags.Add(tags[2]);
            contacts[2].Tags.Add(tags[3]);

            contacts[0].Emails.Add(emails[0]);
            contacts[1].Emails.Add(emails[1]);
            contacts[1].Emails.Add(emails[2]);
            contacts[2].Emails.Add(emails[3]);

            contacts.ForEach(c => context.Contacts.Add(c));
            context.SaveChanges();
        }
    }
}