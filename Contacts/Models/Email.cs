﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contacts.Models
{
    public class Email
    {
        public Email()
        {
            
        }
        public int Id { get; set; }
        public string EmailAdress { get; set; }

        public virtual Contact Contact { get; set; }
    }
}