﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contacts.Models
{
    public class ContactDto
    {

        public ContactDto()
        {
        }

        public static ContactDto FromContact(Contact contact)
        {
            return new ContactDto
            {
                Id = contact.Id,
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                HomeAdress = contact.HomeAdress,

                PhoneNumbers = contact.PhoneNumbers
                    .Select(PhoneNumberDto.FromPhoneNumber)
                    .ToList(),

                Emails = contact.Emails
                    .Select(EmailDto.FromEmail)
                    .ToList(),

                Tags = contact.Tags
                    .Select(TagDto.FromTag)
                    .ToList()

            };
        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string HomeAdress { get; set; }

        public virtual ICollection<PhoneNumberDto> PhoneNumbers { get; set; }
        public virtual ICollection<EmailDto> Emails { get; set; }
        public virtual ICollection<TagDto> Tags { get; set; }
    }
}