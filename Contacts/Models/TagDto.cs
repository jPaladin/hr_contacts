﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contacts.Models
{
    public class TagDto
    {
        public static TagDto FromTag(Tag tag)
        {
            return new TagDto
            {
                Id = tag.Id,
                Name = tag.Name
            };
        }

        public int Id { get; set; }
        public string Name { get; set; }

        //public virtual ICollection<Contact> Contacts { get; set; }

    }
}