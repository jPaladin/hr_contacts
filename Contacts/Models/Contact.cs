﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contacts.Models
{
    public class Contact
    {
        public Contact()
        {
            this.PhoneNumbers = new HashSet<PhoneNumber>();
            this.Tags = new HashSet<Tag>();
            this.Emails = new HashSet<Email>();
        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string HomeAdress { get; set; }

        public virtual ICollection<PhoneNumber> PhoneNumbers { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<Email> Emails { get; set; }
    }
}