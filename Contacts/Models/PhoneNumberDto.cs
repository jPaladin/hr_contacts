﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contacts.Models
{
    public class PhoneNumberDto
    {
        public PhoneNumberDto()
        {

        }

        public static PhoneNumberDto FromPhoneNumber(PhoneNumber phoneNumber)
        {
            return new PhoneNumberDto
            {
                Id = phoneNumber.Id,
                Number = phoneNumber.Number
                //ContactId = null
            };
        }

        public int Id { get; set; }
        public string Number { get; set; }
    }
}