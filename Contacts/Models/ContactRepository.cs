﻿using Contacts.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contacts.Models
{
    public class ContactRepository
    {
        public IEnumerable<Contact> GetAll()
        {
            using (var context = new ContactsContext())
            {
                return context.Contacts.Include("PhoneNumbers").Include("Emails").Include("Tags").ToList();
            }
        }

        public IEnumerable<Contact> SearchByTags(string tagIds)
        {
            using (var context = new ContactsContext())
            {
                  return context.Contacts.Include("PhoneNumbers").Include("Emails").Include("Tags")
                      .Where(c => c.Tags.Any(t => tagIds.Contains(t.Id.ToString()))).ToList();
            }
        }
        public IEnumerable<Contact> SearchName(string name)
        {
            using (var context = new ContactsContext())
            {
                return context.Contacts.Include("PhoneNumbers").Include("Emails").Include("Tags")
                    .Where(c => ((c.FirstName + c.LastName).StartsWith(name) || (c.LastName + c.FirstName).StartsWith(name))).ToList();
            }
        }

        public Contact AddContact(ContactDto contact)
        {
            Contact newContact = new Contact
            {
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                HomeAdress = contact.LastName
            };

            using (var context = new ContactsContext())
            {
                var phoneNumbers = new List<PhoneNumber>();
                foreach (var num in contact.PhoneNumbers)
                {
                    phoneNumbers.Add(new PhoneNumber { Number = num.Number });
                }

                phoneNumbers.ForEach(p => context.PhoneNumbers.Add(p));
                context.SaveChanges();

                var tags = new List<Tag>();
                foreach (var tag in contact.Tags)
                {
                    tags.Add(new Tag { Name = tag.Name });
                }

                tags.ForEach(p => context.Tags.Add(p));
                context.SaveChanges();

                var emails = new List<Email>();
                foreach (var email in contact.Emails)
                {
                    emails.Add(new Email { EmailAdress = email.EmailAdress });
                }

                emails.ForEach(p => context.Emails.Add(p));
                context.SaveChanges();

                context.Contacts.Add(newContact);
                context.SaveChanges();

                newContact.PhoneNumbers = phoneNumbers;
                newContact.Emails = emails;
                newContact.Tags = tags;
                context.SaveChanges();
            }

            return newContact;

        }

        public void RemoveTag(int tagId)
        {
            using (var context = new ContactsContext())
            {
                var tagToRemove = context.Tags.First(t => t.Id == tagId);
                context.Tags.Remove(tagToRemove);
                context.SaveChanges();           
            }
        }

        public Contact AddNewContact(Contact contact)
        {
        
            using (var context = new ContactsContext())
            {
                context.Contacts.Add(contact);
                context.SaveChanges();
            }

            return contact;

        }
        public Contact GetContact(int id)
        {
            using (var context = new ContactsContext())
            {
                return context.Contacts.Include("PhoneNumbers").Include("Emails").Include("Tags")
                    .Where(c => c.Id == id).First();
            }
        }
        public void  DeleteContact(int id)
        {
            var contact = GetContact(id);
            if (contact == null) return;
            using (var context = new ContactsContext())
            {
             
                context.Contacts.Attach(contact);
                context.Contacts.Remove(contact);
         
                var phoneNumbers = context.PhoneNumbers.Where(p => p.Contact.Id == id).ToList();
                phoneNumbers.ForEach(p => context.PhoneNumbers.Remove(p));

                var emails = context.Emails.Where(e => e.Contact.Id == id).ToList();
                emails.ForEach(e => context.Emails.Remove(e));

                context.SaveChanges();

            }
            return;

        }
        public Contact SaveContact(ContactDto contact)
        {
            using (var context = new ContactsContext())
            {

                var saved = context.Contacts.Include("PhoneNumbers").Include("Emails").Include("Tags")
                    .Where(c => c.Id == contact.Id).First();
                saved.FirstName = contact.FirstName;
                saved.LastName = contact.LastName;
                saved.HomeAdress = contact.HomeAdress;

                //Find all numbers that are not in new data and remove them
                saved.PhoneNumbers.Where(p => !contact.PhoneNumbers.Select(x => x.Id).ToList().Contains(p.Id))
                    .ToList().ForEach(p => context.PhoneNumbers.Remove(p));

                var phoneNumbers = new List<PhoneNumber>();
                foreach (var num in contact.PhoneNumbers)
                {
                    if (num.Id == 0)
                    {
                        phoneNumbers.Add(new PhoneNumber() { Number = num.Number });
                    }
                    else
                    {
                        var change = saved.PhoneNumbers.Where(n => n.Id == num.Id).First();
                        change.Number = num.Number;
                        context.SaveChanges();
                    }
                }

                phoneNumbers.ForEach(p => { context.PhoneNumbers.Add(p); saved.PhoneNumbers.Add(p); });


                //Find all emails that are not in new data and remove them
                saved.Emails.Where(e => !contact.Emails.Select(x => x.Id).ToList().Contains(e.Id))
                    .ToList().ForEach(e => context.Emails.Remove(e));

                var emails = new List<Email>();
                foreach (var email in contact.Emails)
                {

                    if (email.Id == 0)
                    {
                        emails.Add(new Email() { EmailAdress = email.EmailAdress });
                    }
                    else
                    {
                        var change = saved.Emails.Where(e => e.Id == email.Id).First();
                        change.EmailAdress = email.EmailAdress;
                        context.SaveChanges();
                    }
                }

                emails.ForEach(e => { context.Emails.Add(e); saved.Emails.Add(e); });

                var tags = new List<Tag>();
                foreach (var tag in contact.Tags)
                {
                    if (tag.Id == 0)
                    {
                        tags.Add(new Tag() { Name = tag.Name });
                    }
                    else if (!saved.Tags.Any(t=> t.Id== tag.Id)) {
                        saved.Tags.Add(context.Tags.Where(t => t.Id == tag.Id).First());
                    }



                }

                tags.ForEach(p => { context.Tags.Add(p); saved.Tags.Add(p); });
                 //Find all tags that are not in new data and remove them
                 saved.Tags.Where(t => !contact.Tags.Select(x => x.Id).ToList().Contains(t.Id))
                     .ToList().ForEach(t => saved.Tags.Remove(t));

                context.SaveChanges();

            
                return saved;
            }

        }
        public static IEnumerable<TagDto> GetAllTags()
        {
            using (var context = new ContactsContext())
            {
                return context.Tags.Select(TagDto.FromTag).ToList();
            }
        }

    }
}