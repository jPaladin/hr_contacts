﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Contacts.Models
{
    public class EmailDto
    {
        public static EmailDto FromEmail(Email email)
        {
            return new EmailDto
            {
                Id = email.Id,
                EmailAdress = email.EmailAdress

            };

        }

        public int Id { get; set; }

        [DataType(DataType.EmailAddress)]
        public string EmailAdress { get; set; }
    }
}