﻿angular.module('Contacts.contactsController', [])
    .controller("contactsController", ['$scope', '$http', '$filter', function ($scope, $http, $filter) {

        $scope.getAll = function () {
            $http.get("/api/contacts/get-all").success(function (data) {
                $scope.model = data;
            });

            //Get All Tags
            $http.get("/api/contacts/util").success(function (data) {
                $scope.allTags = data;
            });
        }

        $scope.getAll();

        $scope.contactEdit = {};
        $scope.queryTags = [];
        //Functions for editting contacts
        $scope.editContact = function (id) {
            $scope.contactEdit = $filter('filter')(angular.copy($scope.model), { Id: parseInt(id) }, true)[0];
        }

        $scope.saveContact = function (id) {
            $scope.newContact = $filter('filter')($scope.model, { Id: parseInt(id) }, true)[0];
            $scope.newContact.FirstName = $scope.contactEdit.FirstName;
            $scope.newContact.LastName = $scope.contactEdit.LastName;
            $scope.newContact.PhoneNumbers = $scope.contactEdit.PhoneNumbers;
            $scope.newContact.Emails = $scope.contactEdit.Emails;
            $scope.newContact.HomeAdress = $scope.contactEdit.HomeAdress;
            $scope.newContact.Tags = $scope.contactEdit.Tags;
            $scope.contactEdit = {};
            $http.post('api/contacts/edit-contact', $scope.newContact).then((function (data) {
                $scope.newContact = data;
                console.log(data)
            }));

        }
        $scope.addNumber = function (edit) {
            edit.PhoneNumbers.push({ "Id": null, "Number": "" })
        }
        $scope.removeNumber = function (number) {
            var index = $scope.contactEdit.PhoneNumbers.indexOf(number)
            $scope.contactEdit.PhoneNumbers.splice(index, 1);
        }
        $scope.addEmail = function (edit) {
            edit.Emails.push({ "Id": null, "EmailAdress": "" })
        }
        $scope.removeEmail = function (email) {
            var index = $scope.contactEdit.Emails.indexOf(email)
            $scope.contactEdit.Emails.splice(index, 1);
        }
        $scope.removeTag = function (tag) {
            if (tag == null) return;
            var index = $scope.contactEdit.Tags.indexOf(tag)
            $scope.contactEdit.Tags.splice(index, 1);
        }
        $scope.addTag = function (newTagName) {
            if (newTagName.trim() != "") {
                var exists = $filter('filter')(angular.copy($scope.contactEdit.Tags), { Name: newTagName }, true)[0];
                if (!exists) {
                    $scope.contactEdit.Tags.push({ "Id": null, "Name": newTagName });
                    $scope.allTags.push({ "Id": null, "Name": newTagName });
                }
            }

            $scope.newTagName = "";
        }
        $scope.addTagFromAll = function (tagAll) {
            var exists = $filter('filter')(angular.copy($scope.contactEdit.Tags), { Name: tagAll.Name }, true)[0];
            if (!exists) {
                $scope.contactEdit.Tags.push({ "Id": tagAll.Id, "Name": tagAll.Name });
            }
        }
        $scope.createNew = function () {
            
            $http.get("/api/contacts/add-contact").success(function (data) {
                $scope.model = data;
            });
            $scope.contactEdit = {};

        }
        $scope.removeContact = function (contact) {
            $http.post('/api/contacts/remove-contact', JSON.stringify(contact.Id.toString())).then((function (data) {
                $scope.getAll();
                $scope.contactEdit = {};
            }));
               
        }
        //Functions for searching
        $scope.nameQuery = "";
        $scope.searchByName = function () {
            if ($scope.nameQuery.trim() == "") {
                $scope.getAll();
            } else {
                $http.get("/api/contacts/search-name/" + $scope.nameQuery + "").success(function (data) {
                    $scope.model = data;
                });
            }
        }
        $scope.searchByTag = function (allTagId) {
            var index = $scope.queryTags.indexOf(allTagId);
            if (index > -1) {
                $scope.queryTags.splice(index, 1);
            }
            else {
                $scope.queryTags.push(allTagId);
            }

            if ($scope.queryTags.length == 0) {
                $scope.getAll();
            }
            else {
                $http.get("/api/contacts/search-tag/" + $scope.queryTags + "").success(function (data) {
                    $scope.model = data;

                });
            }

            console.log($scope.queryTags);
        }

        $scope.showTags = 0;
        $scope.toggleSearchByTag = function(){
            if($scope.showTags == 0) $scope.showTags = 1;
            else $scope.showTags = 0;
        }
    }
    ]);
