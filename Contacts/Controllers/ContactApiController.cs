﻿using Contacts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;


namespace Contacts.Controllers
{
    [RoutePrefix("api/contacts")]
    public class ContactApiController : ApiController
    {
        private readonly ContactRepository contactRepository;

        public ContactApiController()
        {
            //implement DI
            this.contactRepository = new ContactRepository();
        }

        [HttpGet]
        [Route("get-all")]
        public IEnumerable<ContactDto> GetAll()
        {
            return this.contactRepository.GetAll().Select(ContactDto.FromContact);
        }

        [HttpGet]
        [Route("search-tag/{tagIds}")]
        public IEnumerable<ContactDto> SearchByTag(string tagIds)
        {
            return this.contactRepository.SearchByTags(tagIds).Select(ContactDto.FromContact);
        }

        [HttpGet]
        [Route("search-name/{name}")]
        public IEnumerable<ContactDto> SearchName(string name)
        {
            return this.contactRepository.SearchName(name).Select(ContactDto.FromContact);
        }
        [HttpGet]
        [Route("add-contact")]
        public IEnumerable<ContactDto> AddContact()
        {
            var newContact = contactRepository.AddNewContact(new Contact() { FirstName = "New Contact" });

            return GetAll();
        }

        [HttpPost]
        [Route("edit-contact")]
        public System.Web.Mvc.ActionResult EditContact(ContactDto contact)
        {
            var saved = contactRepository.SaveContact(contact);
            return new System.Web.Mvc.JsonResult() { Data = ContactDto.FromContact(saved), JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        [Route("remove-contact")]
        public System.Web.Mvc.ActionResult RemoveContact([FromBody] string contactId)
        {
            contactRepository.DeleteContact(Int32.Parse(contactId));
            return new System.Web.Mvc.JsonResult() { Data = contactId, JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet };

        }
        [HttpPost]
        [Route("remove-tag")]
        public System.Web.Mvc.ActionResult RemoveTag([FromBody] string tagId)
        {
            contactRepository.RemoveTag(Int32.Parse(tagId));
            return new System.Web.Mvc.JsonResult() { Data = tagId, JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet };

        }

        [HttpGet]
        [Route("util")]
        public IEnumerable<TagDto> GetAllTags()
        {
            return ContactRepository.GetAllTags();
        }



    }
}
